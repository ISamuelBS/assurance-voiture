import { Component, OnInit } from '@angular/core';
import { Auto } from './auto';
import { Cliente } from './cliente';
import { AutoService } from './auto.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2'

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {

  private auto: Auto = new Auto();
  private cliente: Cliente = new Cliente();

  marcas: any[] = [];
  modelos: any[] = [];
  submarcas: any[] = [];
 
  disabledSubMarca = false;

  constructor(private autoService: AutoService, 
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  descripcion = "Ingresa los datos de tu auto y encuentra el mejor precio comparando todas las aseguradoras en México.";
  privacidad = "Acepto que he leído el  aviso de privacidad.";
  boton = "Ver precios de mi seguro.";
  footer = "El Mejor Comparador de Seguros de Autos, Compara en 10 Segundos";

  ngOnInit() {
    this.cargarMarcas();

    this.cargarModelos();

    if (this.auto.marca && this.auto.modelo) {
      this.disabledSubMarca = false;
    }
  }

  cargarMarcas(): void {
    this.autoService.getMarcas()
    .subscribe(
      (data) => { // Conexión
        this.marcas = data['Results'];
      },
      (error) => {
        console.error(error);
      }
      );
  }


  cargarSubMarcas(): void {
    this.autoService.getSubMarcas(this.auto.marca, this.auto.modelo)
    .subscribe(
      (data) => { // Conexión
        this.submarcas = data['Results'];
      },
      (error) => {
        console.error(error);
      }
    );
  }


  cargarModelos(): void {
    for (let i = 0; i < 32; i++){
      this.modelos[i] = 1990 + i;
    }
  }

  guardarCliente(): void {
    localStorage.setItem('datos', JSON.stringify(this.cliente));
    this.router.navigate(['/lista']);
    swal.fire(
      'Vamos a cotizar tu seguro', 
      `Tu información: 
      Nombre: ${this.cliente.nombre}
      Email: ${this.cliente.email} 
      Telefono: ${this.cliente.telefono}
      guardado con exito`, 
      'success');
  }

  guardarAuto(): void {
    localStorage.setItem('datos', JSON.stringify(this.auto));
  }

}

