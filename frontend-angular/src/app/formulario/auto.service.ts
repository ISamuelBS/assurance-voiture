import { Injectable } from '@angular/core';
import { Auto } from './auto';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AutoService {

  private urlEndPointMarcas: string = 'https://vpic.nhtsa.dot.gov/api/vehicles/GetMakesForVehicleType/car?format=json';
  private urlEndPointSubMarcas: string = 'https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformakeyear/make/';
  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(protected http: HttpClient) { }

  getMarcas(): Observable<any> {
  return this.http.get(this.urlEndPointMarcas);
  }

  getSubMarcas(marca, modelo): Observable<any>{
  	return this.http.get<Auto>(`${this.urlEndPointSubMarcas}${marca}/modelyear/${modelo}/?format=json`);
  }

}
