export class Cliente {
  id: number;
  edad: string;
  sexo: string;
  codigoPostal: string;
  nombre: string;
  email:string;
  telefono:string;
}