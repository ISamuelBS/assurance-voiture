export class Auto {
  id: number;
  marca: string;
  modelo: string;
  submarca: string;
  version: string;
}
