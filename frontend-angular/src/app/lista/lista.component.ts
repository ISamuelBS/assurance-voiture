import { Component, OnInit } from '@angular/core';
import { Auto } from '../formulario/auto';
import { Cliente } from '../formulario/cliente';
import { Cotizacion } from './cotizacion';
import { COTIZACIONES } from './cotizaciones.json';
import { AutoService } from '../formulario/auto.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2'


@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  cliente: Cliente[];
  cotizaciones: Cotizacion[];
  local;
 

    constructor(private autoService: AutoService, 
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  	this.local = localStorage.getItem('datos');
  	this.cliente = JSON.parse(this.local);
  	this.cotizaciones = COTIZACIONES;
  }
}
